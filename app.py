from flask import Flask, request, render_template, jsonify
from translate import Translator

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def translate_text():
    if request.method == 'POST':
        text = request.form['text']
        to_lang = request.form['language']
        
        translator = Translator(to_lang=to_lang, from_lang='autodetect')
        translated_text = translator.translate(text)
        
        # Renvoyer une réponse JSON contenant le texte traduit
        return jsonify(translated_text=translated_text)

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
