# Démarrage

 - projet sur gitlab : https://gitlab.com/maxlefort35/trad
 - Suivi via Trello : https://trello.com/b/42OyiWY9/pipeline-app-trad

## Installation de modules
1. Utiliser la commande : `pip install Flask` pour installer Flask.
2. Utiliser la commande : `pip install translate` pour installer la bibliothèque de traduction.

## Lancer avec Flask
1. Exécuter la commande : `export FLASK_APP=app.py` une seule fois pour définir l'application Flask. `get FLASK_APP=app.py` pour windows.
2. Lancer Flask avec la commande : `flask run`.

## Lancer les tests avec Unittest
1. Avoir un fichier "tests" à la racine du projet, il contiendra nos tests, et respecter la convention de nommage.
2. Utiliser la commande : `python -m unittest discover tests` en local pour avoir le resultat des tests.

## Lancer avec un container Docker via le Dockerfile en local
1. Construire l'image Docker : `docker build -t <nom-de-l-image> .`
2. Exécuter l'image Docker dans un conteneur : `docker run --name <nom-du-container> -p 5000:5000 <nom-de-l-image>`
3. Dans le navigateur taper : `http://localhost:5000/`

## Lancer avec un container Docker depuis l'image du dockerhub
1. Pull de l'image depuis le repository : `docker pull maxilefort/imagetrad`
2. Exécuter l'image Docker dans un conteneur : `docker run --name <nom-du-container> -p 5000:5000 maxilefort/imagetrad`
3. Dans le navigateur taper : `http://localhost:5000/`

# Documentation et spec

### 1. Objectifs

Créer une application web de traduction de textes qui permet à l'utilisateur de traduire un texte en diverses langues et l’incorporer dans une pipeline CI/CD.

### 2. Fonctionnalités

- Sélection de la langue cible pour la traduction.
- Affichage du texte traduit sur la même page.
- Traduction à partir d’un fichier
- Possibilité de télécharger le fichier traduit

### 3. Technologies

- Langage de programmation : Python 3.x
- Framework : Flask
- Module : translate
- Système de gestion de version : Git
- Intégration continue/déploiement continu : GitLab
- Infrastructure : Docker

### 4. Mise en œuvre et développement

- Développer une application web en utilisant Python/Flask.
- Utiliser le module Python "translate" pour la traduction du texte.
- Créer une interface utilisateur en utilisant HTML pour permettre à l'utilisateur de saisir le texte et de sélectionner la langue cible pour la traduction.
- Configurer GitLab pour la CI/CD, pour la génération d'images Docker, et pour le déploiement sur Docker Hub.

#### Features possibles :

- Traduction de textes à partir d'une page web en utilisant un formulaire HTML.
- Traduire à partir d’un fichier audio.
- Possibilité d’échanger les champs à l’aide d’un bouton.
- Ajouter à la pipeline un système d’envoi mail en cas d’échec de celle-ci.

### 5. Tests

- Tester l'application avec Pytest pour s'assurer que la traduction est correcte et fonctionne normalement.

### 6. Déploiement

- Déployer l'image Docker sur Docker Hub pour permettre à d'autres utilisateurs d'utiliser l'application.

### 7. Critères de réussite

1. Fonctionnalité de traduction: 
    
    L'application doit être capable de traduire correctement du texte d'une langue à une autre.
    
2. Interfaçage utilisateur: 
    
    L'interface utilisateur doit être facile à utiliser et permettre à l'utilisateur de saisir du texte à traduire et de sélectionner la langue de traduction.
    
3. Qualité du code: 
    
    Le code doit être bien structuré, lisible.
    
4. Tests automatisés: 
    
    Des tests automatisés doivent être mis en place pour garantir le bon fonctionnement de l'application



