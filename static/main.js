
        $(document).ready(function() {
            $('#translate-form').submit(function(event) {
                event.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '/',
                    data: $('#translate-form').serialize(),
                    success: function(response) {
                        $('#translated_text').text(response.translated_text);
                    }
                });
            });
        });
  