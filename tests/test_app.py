import os
import sys
import unittest

# Récupére le chemin absolu vers le dossier parent du fichier de test
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

# Ajoute le chemin vers la racine du projet au sys.path
sys.path.insert(0, parent_dir)

from app import app

class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_translate_text(self):
        response_en = self.app.post('/', data=dict(text='bonjour', language='en'))
        response_fr = self.app.post('/', data=dict(text='hello', language='fr'))
        self.assertIn(b'hello', response_en.data)
        self.assertIn(b'bonjour', response_fr.data)


if __name__ == '__main__':
    unittest.main()