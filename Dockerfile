# Utiliser une image alpine de Python
FROM python:3.9-alpine

# Copier les fichiers dans le conteneur
COPY . /app

# Définir le répertoire de travail
WORKDIR /app

# Installation des dépendances pour l'application Flask
RUN apk add --no-cache gcc musl-dev linux-headers && \
    pip install --no-cache-dir -r requirements.txt && \
    apk del gcc musl-dev linux-headers

# Variable d'environnement FLASK_APP
ENV FLASK_APP=app.py

# Exposer le port 
EXPOSE 5000

# Exécuter l'application Flask au démarrage du conteneur
CMD ["flask", "run", "--host=0.0.0.0"]
